import java.util.*;

public class Main {
    public static void main(String[] args) {
        ISort <Integer> instance = new Sort<Integer>();

//		Collection <Integer> inputCollection = Util.toCollection(6, 3, 5, 7, 1, 4, 2);
        ArrayList <Integer> input = new ArrayList<Integer>();
        input.add(6);
        input.add(3);
        input.add(5);
        input.add(7);
        input.add(1);
        input.add(4);
        input.add(2);

        Integer[]sortedInput=new Integer[input.size()];
        input.toArray(sortedInput);
        Arrays.sort(sortedInput);
        IHeap <Integer> sorted=instance.heapSort(input);
        ArrayList <Integer> bfs=new ArrayList <Integer>();

        Queue<INode	<Integer>> q = new LinkedList<INode <Integer>>();
        q.add(sorted.getRoot());
        while(!q.isEmpty()) {
            INode <Integer> current = q.poll();
            bfs.add(current.getValue());
            if (current.getLeftChild() != null)
                q.add(current.getLeftChild());
            if (current.getRightChild() != null)
                q.add(current.getRightChild());
        }
        boolean sortedFlag = true;
        for(int i = 0;i < sortedInput.length&&sortedFlag;++i) {
            System.out.println(bfs.get(i));
            System.out.println(sortedInput[i]);
            sortedFlag &= sortedInput[i] == bfs.get(i);
        }

        System.out.println(sortedFlag);
    }

}
