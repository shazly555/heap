
import java.util.ArrayList;

public class Node <T extends Comparable<T>> implements INode {
    ArrayList<INode> heapArray = new ArrayList();
    int index;
    T value;



    public void setIndex(int index) {
        this.index = index;
    }


    public Node(ArrayList<INode> heapArray, int index, Comparable value) {
        this.heapArray = heapArray;
        this.index = index;
        this.value = (T) value;

        /*if (2 * index + 1 < heapArray.size()) {
            left = new Node(this, heapArray, 2 * index + 1);
        }
        if (2 * index + 2 < heapArray.size()) {
            right = new Node(this, heapArray, 2 * index + 2);
        }*/
    }

    public Node() {
    }

    @Override
    public INode getLeftChild() {
        // System.out.print(heapArray.size());

        if (2 * index + 1 < heapArray.size()) {
            return heapArray.get(2 * index + 1);
        }
        return null;
    }

    @Override
    public INode getRightChild() {

        if ( 2 * index + 2 < heapArray.size()) {
            return heapArray.get(2 * index + 2);
        }
        return null;
    }

    @Override
    public INode getParent() {
        return heapArray.get((index - 1) / 2);

    }

    @Override
    public T getValue() {
        return value;
    }

    @Override
    public void setValue(Comparable value) {
        this.value = (T) value;
    }


}
