
import org.omg.CosNaming.NamingContextExtPackage.StringNameHelper;

import java.awt.List;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.*;

public class HeapTree <T extends Comparable<T>> implements IHeap {
    ArrayList<INode> heapArray = new ArrayList();
    Node root = new Node( heapArray, 0, 0);
    int size = 0;
    @Override
    public INode getRoot() {
        return root;
    }

    public void setHeapArray(ArrayList<INode> heapArray) {
        this.heapArray = heapArray;
    }

    public void setRoot(Node root) {
        this.root = root;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public int size() {
        return size;
    }


    @Override
    public void heapify(INode node) {
        INode left = node.getLeftChild();
        INode right = node.getRightChild();
        INode maxNode = node;

        if (left != null && maxNode.getValue().compareTo(left.getValue()) < 0) {
            maxNode = left;
        }
        if (right != null && maxNode.getValue().compareTo(right.getValue()) < 0) {
            maxNode = right;
        }

        if (maxNode != node) {
            swap(node, maxNode);
            heapify(maxNode);
        }
    }

    public void heapifyArrayImplementation(int index, Comparable[] unorderheap) {

        int maxNode = index;

        if (index * 2 + 1 < unorderheap.length && unorderheap[maxNode].compareTo(unorderheap[index * 2 + 1]) < 0) {
            maxNode = index * 2 + 1;
        }  if (index * 2 + 2 < unorderheap.length && unorderheap[maxNode].compareTo(unorderheap[index * 2 + 2]) < 0) {
            maxNode = index * 2 + 2;
        }

        if (maxNode != index) {

            Comparable temp = unorderheap[index];
            unorderheap[index] = unorderheap[maxNode];
            unorderheap[maxNode] = temp;
            heapifyArrayImplementation(maxNode, unorderheap);
        }
    }

    public Collection getHeapArray() {
        return heapArray;
    }

    @Override
    public Comparable extract() {

        if (size == 0) {
            return null;
        }

        T goal = (T) heapArray.get(0).getValue();
        heapArray.get(0).setValue(heapArray.get(heapArray.size() - 1).getValue());
        heapArray.remove(heapArray.size() - 1);
        size--;
        if (size != 0) {
            heapify(heapArray.get(0));
        }

        return goal;
    }

    public void moveup(int index) {
        if (index == 0) {
            return;
        }
        if (heapArray.get((index - 1) / 2).getValue().compareTo(heapArray.get(index).getValue()) < 0) {

            Comparable temp = heapArray.get(index).getValue();
            heapArray.get(index).setValue(heapArray.get((index - 1) / 2).getValue());
            heapArray.get((index - 1) / 2).setValue(temp);
            moveup((index - 1) / 2);

        }
    }

    @Override
    public void insert(Comparable element) {

        if (size == 0) {
            root = new Node(heapArray, 0,element);
        }
        heapArray.add(new Node(heapArray, heapArray.size(), element));
        moveup(heapArray.size() - 1);
        size++;

    }

/*    public Node makeTree( Node root, int index, Comparable[] unorderHeap) {
        if (index * 2 + 1 < unorderHeap.length) {
            Node node = new Node(heapArray,index * 2 + 1, unorderHeap[index * 2 + 1]);
            heapArray.add(node);
            makeTree(node, index * 2 + 1, unorderHeap);
        }

        if (index * 2 + 2 < unorderHeap.length) {
            root.setRight(new Node(heapArray,index * 2 + 2,  unorderHeap[index * 2 + 1]));
            makeTree((Node) (root).getRightChild(), index * 2 + 2, unorderHeap);
        }
        return root;
    }*/

    @Override
    public void build(Collection unordered) {
//        File folder = new File(System.getProperty("user.dir") + "//eg//edu//alexu//csd//filestructure");
//        File[] listOfFiles = folder.listFiles();
//        String s = new String();
//        for (int i = 0; i < listOfFiles.length; i++) {
//            if (listOfFiles[i].isFile()) {
//                s += "File " + listOfFiles[i].getName();
//            } else if (listOfFiles[i].isDirectory()) {
//                s += "Directory " + listOfFiles[i].getName();
//            }
//        }
//        throw new RuntimeException(s);
//
/*

        try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader =
                    new FileReader(System.getProperty("user.dir") + "//eg//edu//alexu//csd//filestructure//test//sort//SmokeTest.java");

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader =
                    new BufferedReader(fileReader);
            String line = new String();

            String s = new String();
            while((line = bufferedReader.readLine()) != null) {
                s += line;
            }

            throw new RuntimeException(s);
            // Always close files.
        }
        catch(Exception ex) {
            throw new RuntimeException(ex.getMessage());
        }
*/

        Comparable[] unorderHeap = new Comparable[unordered.size()];
        int counter = 0;
        for (Iterator iterator = unordered.iterator(); iterator.hasNext(); ) {
            unorderHeap[counter++] = (Comparable) iterator.next();
        }


        for (int i = unorderHeap.length / 2 - 1; i > -1; i--) {
            heapifyArrayImplementation(i, unorderHeap);
        }
        heapArray.clear();
        root = null;
        if (unorderHeap.length != 0) {
            root= new Node(heapArray,0,unorderHeap[0]);
            heapArray.add(root);
        }

        for (int i = 1; i < unorderHeap.length; i++) {
            heapArray.add(new Node(heapArray, i, unorderHeap[i]));
        }
        size = unorderHeap.length;
        //   makeTree(root, 0, unorderHeap);

    }

    public void swap(INode node1, INode node2) {
        Comparable value1 = node1.getValue();
        node1.setValue(node2.getValue());
        node2.setValue(value1);
    }

}
