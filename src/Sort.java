import java.util.ArrayList;
import java.util.ArrayList;
import java.util.Collections;

public class Sort<T extends Comparable<T>> implements ISort<T> {

	HeapTree heapTree = new HeapTree();
	public void sort(Integer arr[])
	{
		int n = arr.length;

		// Build heap (rearrange array)
		for (int i = n / 2 - 1; i >= 0; i--)
			heapify(arr, n, i);

		// One by one extract an element from heap
		for (int i=n-1; i>=0; i--)
		{
			// Move current root to end
			int temp = arr[0];
			arr[0] = arr[i];
			arr[i] = temp;

			// call max heapify on the reduced heap
			heapify(arr, i, 0);
		}
		heapTree.heapArray.clear();
		for (int i = 0; i < arr.length; i++) {
			heapTree.heapArray.add(arr[i]);
		}
	}
	void heapify(Integer arr[], int n, int i)
	{
		int largest = i;  // Initialize largest as root
		int l = 2*i + 1;  // left = 2*i + 1
		int r = 2*i + 2;  // right = 2*i + 2

		// If left child is larger than root
		if (l < n && arr[l] > arr[largest])
			largest = l;

		// If right child is larger than largest so far
		if (r < n && arr[r] > arr[largest])
			largest = r;

		// If largest is not root
		if (largest != i)
		{
			int swap = arr[i];
			arr[i] = arr[largest];
			arr[largest] = swap;

			// Recursively heapify the affected sub-tree
			heapify(arr, n, largest);
		}
	}

	@Override
	public IHeap<T> heapSort(ArrayList<T> unordered) {

		heapTree.build(unordered);
		unordered.clear();
		while (heapTree.size != 0) {
			unordered.add((T) heapTree.extract());
		}
		Collections.reverse(unordered);
		ArrayList<INode> heapArray = new ArrayList<>();
		for (int i = 0; i < unordered.size(); i++) {
			heapArray.add(new Node(heapArray, i, unordered.get(i)));
		}
		heapTree.setSize(unordered.size());
		heapTree.setHeapArray(heapArray);
		heapTree.setRoot((Node) heapArray.get(0));
		return heapTree;
	}
	private void swap(ArrayList<T> unordered, int a, int b) {
		T temp = unordered.get(a);
		unordered.set(a, unordered.get(b));
		unordered.set(b, temp);
	}

	private void bubbleSort(ArrayList<T> unordered, int size) {
		boolean swapFlag = true;

		for (int i = 0; i < size && swapFlag; i++) {
			swapFlag = false;
			for (int j = 0; j < size - 1 - i; j++) {
				if (unordered.get(j).compareTo(unordered.get(j + 1)) > 0) {
					swap(unordered, j, j + 1);
					swapFlag = true;
				}
			}
		}
	}

	@Override
	public void sortSlow(ArrayList<T> unordered) {
		bubbleSort(unordered, unordered.size());
	}


	private void quickSort(ArrayList<T> unordered,int lowerIndex, int higherIndex) {

		int i = lowerIndex;
		int j = higherIndex;
		// calculate pivot number, I am taking pivot as middle index number
		Comparable<T> pivot = unordered.get(lowerIndex+(higherIndex-lowerIndex)/2);
		// Divide into two arrays
		while (i <= j) {

			while (unordered.get(i).compareTo((T) pivot) < 0) {
				i++;
			}
			while (unordered.get(j).compareTo((T) pivot) > 0) {
				j--;
			}
			if (i <= j) {
				exchangeNumbers(unordered, i, j);
				//move index to next position on both sides
				i++;
				j--;
			}
		}
		// call quickSort() method recursively
		if (lowerIndex < j)
			quickSort(unordered,lowerIndex, j);
		if (i < higherIndex)
			quickSort(unordered,i, higherIndex);
	}

	private void exchangeNumbers(ArrayList<T> unordered,int i, int j) {
		Comparable<T> temp = unordered.get(i);
		unordered.set(i, unordered.get(j));
		unordered.set(j, (T) temp);
	}
	//merge sort
	@Override
	public void sortFast(ArrayList<T> unordered) {
		//	Collections.sort(unordered);

		quickSort(unordered, 0, unordered.size()-1);


	}
	
	 

	    
	


}